package ia.book;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class BookManager {
    private List<Book> books;

    public List<Book> searchByTitle(String title) {
        return books.stream()
                .filter(b -> b.getTitle().contains(title))
                .collect(Collectors.toList());
    }

    public List<Book> searchByAuthor(String author) {
        return books.stream()
                .filter(b -> b.getAuthor().contains(author))
                .collect(Collectors.toList());
    }

    public Book searchByISBN(String isbn) {
        return books.stream()
                .filter(b -> b.getIsbn().equals(isbn))
                .findAny()
                .orElse(null);
    }
}
