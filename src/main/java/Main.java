import ia.book.Book;
import ia.book.BookManager;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String... args) {
        List<Book> books = new ArrayList<>();
        books.add(new Book("b1", "Koparka dla opornych", "Zdzisław Maliniak", "123456789", 2000, "O'Reily", 256));
        books.add(new Book("b2", "Java rulez", "James Gosling", "2244668800", 1998, "RKRDD", 666));
        books.add(new Book("b1", "Koparka dla opornych", "Zdzisław Maliniak", "123456789", 2000, "Mariany", 423));

        BookManager bm = new BookManager(books);
        System.out.println(bm.searchByTitle("Java"));
        System.out.println(bm.searchByISBN("123456789"));
        System.out.println(bm.searchByAuthor("Maliniak"));
    }
}
